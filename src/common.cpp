// #include <FS.h>
// #ifdef USE_LittleFS
// 	#include <FS.h>
// 	#define SPIFFS LittleFS
// 	#include <LittleFS.h> 
//  #endif
#include <LittleFS.h> 
#include <ArduinoJson.h>
#include"../include/common.hpp"
#include"../include/waterLvlSensorsControler.hpp"
#include"../include/pumpsControler.hpp"
#include"../include/pinControler.hpp"
#include"WiFiManager.h"
#include <BlynkMultiClient.h>

static WiFiClient blynkWiFiClient;
char BLYNK_AUTH_TOKEN[33];// =  " kr_tSG_Q7BUCZ97R-NwBEiJqpojnp3a6";
                              //  BtdQO9YGdf6WkT-02lys-nVnN8SHotiS
                              //  HMSEHIirfs3tOr2__9KWykpffmtgBXH5
char BLYNK_TEMPLATE_ID[13];// = "TMPLE2TLOoFb"
char BLYNK_TEMPLATE_NAME[20];//=  "Quickstart Template"
//HMSEHIirfs3tOr2__9KWykpffmtgBXH

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

namespace common
{
void connectWiFi()
{
    Serial.begin(9600);

    if (LittleFS.begin()) {
      Serial.println("mounted file system");
      if (LittleFS.exists("/config.json")) {
        //file exists, reading and loading
        Serial.println("reading config file");
        File configFile = LittleFS.open("/config.json", "r");
        if (configFile) {
          Serial.println("opened config file");
          size_t size = configFile.size();
          // Allocate a buffer to store contents of the file.
          std::unique_ptr<char[]> buf(new char[size]);

          configFile.readBytes(buf.get(), size);

          DynamicJsonDocument json(1024);
          auto deserializeError = deserializeJson(json, buf.get());
          serializeJson(json, Serial);
          if ( ! deserializeError ) {
            Serial.println("\nparsed json");
            if(!json["BLYNK_AUTH_TOKEN"].isNull() or !json["BLYNK_TEMPLATE_ID"].isNull() or !json["BLYNK_TEMPLATE_NAME"].isNull())
            {
              strcpy(BLYNK_AUTH_TOKEN, json["BLYNK_AUTH_TOKEN"]);
              strcpy(BLYNK_TEMPLATE_ID, json["BLYNK_TEMPLATE_ID"]);
              strcpy(BLYNK_TEMPLATE_NAME, json["BLYNK_TEMPLATE_NAME"]);
            }
            else{
              Serial.println("JSON doesn't contains necessary keys");
            }
          } else {
            Serial.println("failed to load json config");
          }
          configFile.close();
        }
        // /LittleFS.end(); nwm czy musze zamykac
    }
  } else {
    Serial.println("failed to mount FS");
  }
    WiFiManagerParameter custom_blynk_token("token", "blynk token", BLYNK_AUTH_TOKEN, 33);
    WiFiManagerParameter custom_blynk_temlate_id("id", "blynk template id", BLYNK_TEMPLATE_ID, 13);
    WiFiManagerParameter custom_blynk_template_name("name", "blynk name", BLYNK_TEMPLATE_NAME, 20);
    WiFiManager wifi;
    // wifi.resetSettings(); // To usunac
    wifi.setSaveConfigCallback(saveConfigCallback);
    wifi.addParameter(&custom_blynk_token);
    wifi.addParameter(&custom_blynk_temlate_id);
    wifi.addParameter(&custom_blynk_template_name);
    wifi.autoConnect("Blynk");


  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
    strcpy(BLYNK_AUTH_TOKEN, custom_blynk_token.getValue());
    strcpy(BLYNK_TEMPLATE_ID, custom_blynk_temlate_id.getValue());
    strcpy(BLYNK_TEMPLATE_NAME, custom_blynk_template_name.getValue());
    
    Blynk.addClient("WiFi", blynkWiFiClient, 80); 
    printf("GET_VAL %s:",custom_blynk_token.getValue());
    Serial.println();
    Serial.printf("GivenToken: %s",BLYNK_AUTH_TOKEN);
    Serial.println();
    Serial.printf("BlynkId: %s",BLYNK_TEMPLATE_ID);
    Serial.println();
    Serial.printf("Blynk template: %s",BLYNK_TEMPLATE_NAME);
    Serial.println();

 if (shouldSaveConfig) {
    Serial.println("saving config");
 #if defined(ARDUINOJSON_VERSION_MAJOR) && ARDUINOJSON_VERSION_MAJOR >= 6
    DynamicJsonDocument json(1024);
#else
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
#endif
    json["BLYNK_AUTH_TOKEN"] = BLYNK_AUTH_TOKEN;
    json["BLYNK_TEMPLATE_ID"] = BLYNK_TEMPLATE_ID;
    json["BLYNK_TEMPLATE_NAME"] = BLYNK_TEMPLATE_NAME;

    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

#if defined(ARDUINOJSON_VERSION_MAJOR) && ARDUINOJSON_VERSION_MAJOR >= 6
    serializeJson(json, Serial);
    serializeJson(json, configFile);
#else
    json.printTo(Serial);
    json.printTo(configFile);
#endif
    configFile.close();
    //end save
  }
    Blynk.config(BLYNK_AUTH_TOKEN);
}

void checkingWaterLevel(WaterLvlSensorsControler& controler)
{

  static auto state = controler.isWaterLevelReached();
  static auto lastSensor1Val = state;

  if(state != lastSensor1Val)
  {
    Serial.println("[checkingWaterLevels]Water level changed. Sending update to cloud");
    Blynk.virtualWrite(controler.virtualPins[0], state == WaterLvlSensorsControler::States::REACHED ? true : false);
    lastSensor1Val = state;
  }
  
}

void pinControlerTask(void* param)
{
  auto controler_ptr = static_cast<PinControler*>(param);

  while(true)
  {
    auto duty = controler_ptr->getDuty();
    controler_ptr->turnOn();
    Serial.println("[pinControlerTask]Pin turned ON");
    delay(duty * 1000);
    controler_ptr->turnOff();
    Serial.println("[pinControlerTask]Pin turned OFF");
    delay((10-duty) * 1000);

  }
}

}
