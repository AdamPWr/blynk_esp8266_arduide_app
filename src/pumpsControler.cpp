#include"../include/pumpsControler.hpp"

PumpsControler::PumpsControler(u_int8_t pump1Pin, u_int8_t pump2Pin):
PUMP1_PIN(pump1Pin),
PUMP2_PIN(pump2Pin)

{
    pinMode(PUMP1_PIN, OUTPUT);
    pinMode(PUMP2_PIN, OUTPUT);

    digitalWrite(PUMP1_PIN,LOW);
    digitalWrite(PUMP1_PIN,LOW);
}

void PumpsControler::turnOnPump1()
{
    digitalWrite(PUMP1_PIN,HIGH);
}
void PumpsControler::turnOnPump2()
{
    digitalWrite(PUMP2_PIN,HIGH);
}
void PumpsControler::turnOffPump1()
{
    digitalWrite(PUMP1_PIN,LOW);
}
void PumpsControler::turnOffPump2()
{
    digitalWrite(PUMP2_PIN,LOW);
}
void PumpsControler::turnOnAllPumps()
{
    turnOnPump1();
    turnOnPump2();
}
void PumpsControler::turnOffAllPumps()
{
    turnOffPump1();
    turnOffPump2();
}