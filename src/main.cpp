#include <Arduino.h>

#include <ESP8266WiFi.h>
#include"WiFiManager.h"
#include <SPI.h>
#include <BlynkMultiClient.h>

#include"../include/common.hpp"
#include"../include/pinControler.hpp"
#include"../include/pumpsControler.hpp"
#include"../include/waterLvlSensorsControler.hpp"

const std::vector<u_int8_t> waterSensorsVirtualPins{V5}; //[CHANGE]
WaterLvlSensorsControler waterLvlSensorsControler(waterSensorsVirtualPins,2);//[CHANGE]
PumpsControler pumpsControler(5,6);//[CHANGE]
PinControler pinControler(7);//[CHANGE]

int64_t ref_time;


typedef BlynkArduinoClientMulti<Client> BlynkTransport;

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_BLYNK)
  static BlynkTransport   _blynkTransport1;

  BlynkMultiClient<BlynkTransport> Blynk(_blynkTransport1);
#else
  extern BlynkMultiClient<BlynkTransport> Blynk;
#endif

BLYNK_WRITE(V0)// PUMP 1
{
  int pinValue = param.asInt();
  Serial.print("PUMP1 state change to :");
  Serial.println(pinValue);


  if(pinValue == 1) // Switch on PUMP 1
  {
    pumpsControler.turnOnPump1();
  }
  else if(pinValue == 0)
  {
    pumpsControler.turnOffPump1();
  }
  else{
    Serial.println("To control pump 1 on V0 set value 0 or 1");
  }
}

BLYNK_WRITE(V2)// PUMP 2 [CHANGE]
{

  int pinValue = param.asInt();
  Serial.print("PUMP2 state change to :");
  Serial.println(pinValue);

  if(pinValue == 1) // Switch on PUMP 2
  {
    pumpsControler.turnOnPump2();
  }
  else if(pinValue == 0)
  {
    pumpsControler.turnOffPump2();
  }
  else{
    Serial.println("To control pump 2 on V0 set value 0 or 1");
  }

}


BLYNK_WRITE(V7)// slinder for pin ONN and OFF  [CHANGE]
{

  int pinValue = param.asInt();
  Serial.print("PIN Switching slider state change to :");
  Serial.println(pinValue);
  pinControler.updateDuty(pinValue);

}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  ref_time = millis();
}

void loop()
{
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Not conneted");
    common::connectWiFi();
    return;
  }
  common::checkingWaterLevel(waterLvlSensorsControler);

  if(Blynk.connected() and !pinControler.dutyHasBeenInitialized)
  {
    Blynk.syncVirtual(V7);
    pinControler.dutyHasBeenInitialized=true;
    Serial.println("Slider value initialized in main");
  }

 if(pinControler.dutyHasBeenInitialized)
  {
    int64_t current_time = millis();

    if(ref_time + 1000ul * pinControler.getDuty() <= current_time)
    {
      pinControler.turnOff();
    }
    else
    {  
      pinControler.turnOn();
    }
    if(current_time >= ref_time + 1000ul * 10)
    {
      ref_time = current_time;
      Serial.println("Updating current time");
    }
  }


  Blynk.run();
}