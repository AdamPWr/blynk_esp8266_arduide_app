#include"../include/waterLvlSensorsControler.hpp"
#include<sstream>


WaterLvlSensorsControler::WaterLvlSensorsControler(const std::vector<u_int8_t>& vPins,uint8_t pump1Pin):
virtualPins(vPins),
SENSOR1_PIN(pump1Pin)
{
    pinMode(SENSOR1_PIN, INPUT_PULLUP);
}

WaterLvlSensorsControler::States WaterLvlSensorsControler::isWaterLevelReached()
{
    auto temp1 = digitalRead(SENSOR1_PIN);
    std::stringstream str;
    str<< "Water level1: "<< temp1;
    Serial.println(str.str().c_str());
    
    return temp1 ?  States::REACHED : States::NOT_REACHED;
}