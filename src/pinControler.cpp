#include"../include/pinControler.hpp"

PinControler::PinControler(u_int8_t pin ,u_int8_t duty):
PIN_NUMBER(pin),
pinDuty(duty)
{
    pinMode(PIN_NUMBER, OUTPUT);
}

void PinControler::updateDuty(u_int8_t newDuty)
{
    pinDuty = newDuty;
}

u_int8_t PinControler::getDuty()
{
    return pinDuty;
}

void PinControler::turnOn()
{
    digitalWrite(PIN_NUMBER,HIGH);
}

void PinControler::turnOff()
{
    digitalWrite(PIN_NUMBER,LOW);
}