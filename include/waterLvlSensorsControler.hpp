#pragma once
#include<Arduino.h>
#include<utility>
#include<vector>


class WaterLvlSensorsControler
{
    const uint8_t SENSOR1_PIN;

public:

    enum class States
    {
        UNINITIALIZED,
        REACHED,
        NOT_REACHED
    };
    const std::vector<u_int8_t>& virtualPins;
    WaterLvlSensorsControler(const std::vector<u_int8_t>&  vPins,uint8_t sensor1Pin = 2);
    States isWaterLevelReached();

};