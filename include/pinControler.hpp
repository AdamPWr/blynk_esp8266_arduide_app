#pragma once
#include<Arduino.h>
class PinControler
{
    const u_int8_t PIN_NUMBER;
    u_int8_t pinDuty;
public:
    bool dutyHasBeenInitialized = false;
    PinControler(u_int8_t pin ,u_int8_t duty = 0);
    u_int8_t getDuty();
    void updateDuty(u_int8_t newDuty);
    void turnOn();
    void turnOff();
};