#include"../include/waterLvlSensorsControler.hpp"

namespace common
{
    void connectWiFi();
    void tempMeasuringTask(void* param);
    void checkingWaterLevel(WaterLvlSensorsControler& controler);
    void pinControlerTask(void* param);
}