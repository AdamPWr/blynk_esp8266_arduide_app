#pragma once
#include<Arduino.h>

class PumpsControler
{

    const uint8_t PUMP1_PIN;
    const uint8_t PUMP2_PIN;
public:

    PumpsControler(u_int8_t pump1Pin, u_int8_t pump2Pin);

    void turnOnPump1();
    void turnOnPump2();
    void turnOffPump1();
    void turnOffPump2();
    void turnOnAllPumps();
    void turnOffAllPumps();

};